//Функции в программировании нужны для обработки входящих данных и возврата результата различных операций над ними
//Функции принимают аргументы для проведения операций над ними, а так же часто необходимо указывать значение аргумента
// по умолчанию в том случае когда при вызове фунции в аргумент не передается никакое значение и ему присвоится undefined

const calcResult = function (firstNum, secondNum, operation){
    do {
        firstNum = +prompt("Input first number");
        secondNum = +prompt("Input second number");
        operation = prompt("Enter your operation");
    } while (Number.isNaN(firstNum) || Number.isNaN(secondNum) || !firstNum || !secondNum )
        switch (operation) {
            case '+': return firstNum + secondNum
            case '-': return firstNum - secondNum
            case '/': return firstNum / secondNum
            case '*': return firstNum * secondNum
        }
}
console.log(calcResult());